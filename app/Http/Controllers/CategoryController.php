<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
session_start();

class CategoryController extends Controller
{
    public function allCategory(){

        $all_category = DB::table('categoris')->get();
        $manager_category = view('Admin.categories.index')->with('all_category' ,$all_category);
        return view('admin_layout')->with('Admin.categories.index' , $manager_category );
    }
    public function saveCategory(Request $request){
        $data = array();
        $data['name'] = $request->name;
        $data['created_at'] = date('Y-m-d H:i:s');

        DB::table('categoris')->insert($data);
        Session::put('message', 'add category success');
        return Redirect::to('/all-category');
    }

    public function editCategory($category_id){
        $edit_category = DB::table('categoris')->where('id',$category_id)->get();
        $manager_category = view('Admin.categories.edit')->with('edit_category' ,$edit_category);
        return view('admin_layout')->with('Admin.categories.edit' , $manager_category );
    }
    public function updateCategory(Request $request,$category_id){
        $data = array();
        $data['name'] = $request->name;
        $data['updated_at'] = date('Y-m-d H:i:s');

        DB::table('categoris')->where('id',$category_id)->update($data);
        Session::put('message', 'update category success');
        return Redirect::to('/all-category');
    }

    public function deleteCategory($category_id){
        DB::table('products')->where('category_id',$category_id)->delete();
        DB::table('categoris')->where('id',$category_id)->delete();
        Session::put('message', 'delete category success');
        return Redirect::to('/all-category');
    }
}
