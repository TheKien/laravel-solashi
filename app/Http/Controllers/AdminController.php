<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
session_start();

class AdminController extends Controller
{
    public function index(){
        return view('Admin.admin_login');
    }
    public function dashboard(){
        return view('Admin.dashboard');
    }
    public function login(Request $request){
        $email = $request->email;
        $password = md5($request->password);

        $result = DB::table('users')->where('email',$email)->where('password',$password)->where('is_admin','1')->first();
        if($result){
            session::put('name',$result->name);
            return Redirect::to('/dashboard');
        }else{
            session::put('message','email or password failed');
            return Redirect::to('/admin');
        }
    }

    public function logout(){
        session::put('name',null);
        return Redirect::to('/admin');
    }
}
