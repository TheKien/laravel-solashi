<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
session_start();

class HomeController extends Controller
{
    public function index(){
        $latest_product = DB::table('products')->orderBy('created_at','desc')->limit('4')->get();

        $best_product = DB::table('products')->where('is_top','1')->limit('8')->get();

        return view('pages.home')->with('latest_product', $latest_product)->with('best_product', $best_product);
    }
}
