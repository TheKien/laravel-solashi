<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
session_start();

class ProductController extends Controller
{
    public function allProduct(){
        $all_products = DB::table('products')->orderBy('updated_at','desc')->get();
        $manager_product = view('Admin.products.index')->with('all_products' ,$all_products);
        return view('admin_layout')->with('Admin.products.index' , $manager_product);
    }
    public function saveProduct(Request $request){
        $data = array();
        $data['code'] = $request->code;
        $data['name'] = $request->name;
        $data['description'] = $request->description;
        $data['category_id'] = $request->category_id;
        $data['price'] = $request->price;
        $data['is_top'] = $request->is_top;
        $data['on_sale'] = $request->on_sale;
        $data['created_at'] = date('Y-m-d H:i:s');

        DB::table('products')->insert($data);
        Session::put('message', 'add product success');
        return Redirect::to('/all-product');
    }

    public function CreateProduct(){
        $categories = DB::table('categoris')->get();

        return view('Admin.products.create')->with('categories', $categories);
    }

    public function editProduct($product_id){
        $categories = DB::table('categoris')->get();

        $edit_product = DB::table('products')->where('id',$product_id)->get();
        $manager_product = view('Admin.products.edit')->with('edit_product' ,$edit_product)->with('categories', $categories);
        return view('admin_layout')->with('Admin.products.edit' , $manager_product );
    }
    public function updateProduct(Request $request,$product_id){
        $data = array();
        $data['code'] = $request->code;
        $data['name'] = $request->name;
        $data['description'] = $request->description;
        $data['category_id'] = $request->category_id;
        $data['price'] = $request->price;
        $data['is_top'] = $request->is_top;
        $data['on_sale'] = $request->on_sale;
        $data['updated_at'] = date('Y-m-d H:i:s');


        DB::table('products')->where('id',$product_id)->update($data);
        Session::put('message', 'update product success');
        return Redirect::to('/all-product');
    }

    public function deleteProduct($product_id){
        DB::table('products')->where('id',$product_id)->delete();
        Session::put('message', 'delete product success');
        return Redirect::to('/all-product');
    }
}
