@extends('layout')
@section('content')
<section class="hero-section">
         <div class="hero-slider owl-carousel owl-loaded owl-drag">
            <div>
               <img src="{{ ('public/frontend/images/bg-banner2.jpg') }}" alt="">
            </div>
            <div>
               <img src="{{'public/frontend/images/bg-banner1.jpg'}}" alt="">
            </div>
         </div>
   </section>

      <!-- //banner -->
      <!-- Features section -->
    <section class="features-section">
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-4 p-0 feature">
                  <div class="feature-inner">
                      <div class="feature-icon">
                          <img src="{{'public/frontend/images/icons/1.png'}}" alt="#" width="30">
                      </div>
                      <h6>Fast Secure Payments</h6>
                  </div>
              </div>
              <div class="col-md-4 p-0 feature">
                  <div class="feature-inner">
                      <div class="feature-icon">
                          <img src="{{'public/frontend/images/icons/2.png'}}" alt="#" width="30">
                      </div>
                      <h6 class="text-white">Premium Products</h6>
                  </div>
              </div>
              <div class="col-md-4 p-0 feature">
                  <div class="feature-inner">
                      <div class="feature-icon">
                          <img src="{{'public/frontend/images/icons/3.png'}}" alt="#" width="30">
                      </div>
                      <h6>Affordable Delivery</h6>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <div class="top-latest-product-section">
        <div class="container">
            <div class="section-title">
                <h3 class="mt-3 mb-3">LATEST PRODUCTS</h3>
            </div>
            <div class="slider owl-carousel owl-drag owl-loaded">
                @foreach($latest_product as $product)
                    <div class="product-item">
                        <div class="pi-pic border">
                            <div class="tag-new">New</div>
                            <a href="single.html">
                                <img  class="lazy img-latest-product" src="{{'public/frontend/images/pro-1.jpg'}}" alt=""  >
                            </a>
                            <div class="pi-links">
                                <div wire:id="W4AhqNbItyXh078PqnJB" wire:initial-data="{&quot;fingerprint&quot;:{&quot;id&quot;:&quot;W4AhqNbItyXh078PqnJB&quot;,&quot;name&quot;:&quot;add-to-cart&quot;,&quot;locale&quot;:&quot;en&quot;},&quot;effects&quot;:{&quot;listeners&quot;:[]},&quot;serverMemo&quot;:{&quot;children&quot;:[],&quot;errors&quot;:[],&quot;htmlHash&quot;:&quot;9e6d5fc8&quot;,&quot;data&quot;:{&quot;product&quot;:[]},&quot;dataMeta&quot;:{&quot;models&quot;:{&quot;product&quot;:{&quot;class&quot;:&quot;App\\Product&quot;,&quot;id&quot;:603,&quot;relations&quot;:[&quot;photo&quot;,&quot;category&quot;],&quot;connection&quot;:&quot;mysql&quot;}}},&quot;checksum&quot;:&quot;3caead6bfa73d51b855957f63ce1d84d60f1353b6ff82fc900bd02cd95a107ba&quot;}}">
                                    <!-- Add product to cart -->
                                    <form  wire:submit.prevent="addToCart" action="" method="post">
                                        <input type="hidden" name="_token" value="HMbaU0CDuVFBlor0DdoWSw1N5A4Wn9XdTbGBMEmi">
                                        <input type="hidden" name="productId" value="603">
                                        <input type="hidden" name="name" value="{{$product->name}}">
                                        <input wire:model="price" id="price" type="hidden" name="price" value="{{$product->price}}">
                                        <input type="hidden" name="quantity" value="1">
                                        <button type="submit" class="wishlist-btn"><span class="fas fa-cart-arrow-down"></span></button>
                                    </form>
                                    <!-- Add product to wishlist -->
                                    <form wire:submit.prevent="addToWishlist" action="" method="post">
                                        <input type="hidden" name="_token" value="HMbaU0CDuVFBlor0DdoWSw1N5A4Wn9XdTbGBMEmi">
                                        <input type="hidden" name="id" value="603">
                                        <input type="hidden" name="name" value="{{$product->name}}">
                                        <input type="hidden" name="price" value="{{$product->price}}">
                                        <input type="hidden" name="quantity" value="1">
                                        <button type="submit" class="wishlist-btn"><span class="far fa-heart"></span></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="pi-text">
                            <h6>{{$product->price}} $</h6>
                            <a href="single.html"><p>{{$product->name}}</p></a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
      <!--//about -->
<section class="product-filter-section">
        <div class="container">
            <div class="section-title">
                <h3 class="mt-3 mb-3">BROWSE TOP SELLING PRODUCTS</h3>
            </div>
            <div class="row">
                @foreach($best_product as $product)
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-m-6 col-6">
                    <div class="product-item">
                        <div class="pi-pic border">
                            <div class="tag-new">New</div>
                            <a href="single.html">
                                <img  class="lazy img-latest-product" src="{{'public/frontend/images/pro7.jpg'}}" alt=""  >
                            </a>
                            <div class="pi-links">
                                <div wire:id="W4AhqNbItyXh078PqnJB" wire:initial-data="{&quot;fingerprint&quot;:{&quot;id&quot;:&quot;W4AhqNbItyXh078PqnJB&quot;,&quot;name&quot;:&quot;add-to-cart&quot;,&quot;locale&quot;:&quot;en&quot;},&quot;effects&quot;:{&quot;listeners&quot;:[]},&quot;serverMemo&quot;:{&quot;children&quot;:[],&quot;errors&quot;:[],&quot;htmlHash&quot;:&quot;9e6d5fc8&quot;,&quot;data&quot;:{&quot;product&quot;:[]},&quot;dataMeta&quot;:{&quot;models&quot;:{&quot;product&quot;:{&quot;class&quot;:&quot;App\\Product&quot;,&quot;id&quot;:603,&quot;relations&quot;:[&quot;photo&quot;,&quot;category&quot;],&quot;connection&quot;:&quot;mysql&quot;}}},&quot;checksum&quot;:&quot;3caead6bfa73d51b855957f63ce1d84d60f1353b6ff82fc900bd02cd95a107ba&quot;}}">
                                <!-- Add product to cart -->
                                  <form  wire:submit.prevent="addToCart" action="" method="post">
                                    <input type="hidden" name="_token" value="HMbaU0CDuVFBlor0DdoWSw1N5A4Wn9XdTbGBMEmi">
                                    <input type="hidden" name="productId" value="603">
                                    <input type="hidden" name="name" value="{{$product->name}}">
                                    <input wire:model="price" id="price" type="hidden" name="price" value="{{$product->price}}">
                                    <input type="hidden" name="quantity" value="1">
                                    <button type="submit" class="wishlist-btn"><span class="fas fa-cart-arrow-down"></span></button>
                                  </form>
                                  <!-- Add product to wishlist -->
                                  <form wire:submit.prevent="addToWishlist" action="" method="post">
                                        <input type="hidden" name="_token" value="HMbaU0CDuVFBlor0DdoWSw1N5A4Wn9XdTbGBMEmi">
                                        <input type="hidden" name="id" value="603">
                                        <input type="hidden" name="name" value="{{$product->name}}">
                                        <input type="hidden" name="price" value="{{$product->price}}">
                                        <input type="hidden" name="quantity" value="1">
                                        <button type="submit" class="wishlist-btn"><span class="far fa-heart"></span></button>
                                  </form>
                                </div>
                              </div>
                        </div>
                        <div class="pi-text">
                            <h6 class="pr-text">$ {{$product->price}}</h6>
                            <a href="single.html" class="pr-link"><p>{{$product->name}}</p></a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
</section>
@endsection
