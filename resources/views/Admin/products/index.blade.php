@extends('admin_layout')
@section('main-content')


    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Cate_id</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>top</th>
                            <th>sale</th>
                            <th>custom</th>
                        </tr>
                        </thead>
{{--                        <tfoot>--}}
{{--                        <tr>--}}
{{--                            <th>ID</th>--}}
{{--                            <th>Name</th>--}}
{{--                            <th>Created_at</th>--}}
{{--                            <th>updated_at</th>--}}
{{--                        </tr>--}}
{{--                        </tfoot>--}}
                        <tbody>
                        @foreach($all_products as $product)
                            <tr>
                                <td>{{$product->id}}</td>
                                <td>{{$product->category_id}}</td>
                                <td>{{$product->code}}</td>
                                <td>{{$product->name}}</td>
                                <td>{{$product->description}}</td>
                                <td>{{$product->price}}</td>
                                <td>{{$product->is_top}}</td>
                                <td>{{$product->on_sale}}</td>
                                <td>
                                    <a href="{{URL::to('/edit-product/'.$product->id)}}" title="Sửa">
                                        <i class="fa fa-pencil-alt"></i>
                                    </a>
                                    <a href="{{URL::to('/delete-product/'.$product->id)}}" title="Xóa"
                                       onclick="return confirm('Are you sure to delete?')">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

@endsection
