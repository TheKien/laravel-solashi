@extends('admin_layout')
@section('main-content')

    <div class="container-fluid">
        <div class="add-pro">
            @foreach($edit_product as $edit_pro)
                <form method="post" action="{{URL::to('/update-product/'.$edit_pro->id)}}" enctype="multipart/form-data" class="add-pro">
                    {{ csrf_field() }}
                    <div class="form-group ">
                        <label for="category_id">Category:</label>
                        <select name="category_id" id="category_id" class="form-control">
                            <?php
                            foreach ($categories as $category):
                            $selected = '';
                            if (isset($edit_pro->category_id) && $category->id == $edit_pro->category_id) {
                                $selected = 'selected';
                            }
                            ?>
                            <option value="<?php echo $category->id ?>" <?php echo $selected; ?>>
                                <?php echo $category->id.'. '.$category->name ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group ">
                        <label for="code">Code:</label>
                        <input type="text" name="code" value="{{$edit_pro->code}}" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" name="name" value="{{$edit_pro->name}}" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="description">Description:</label>
                        <textarea name="description" id="description" cols="" rows="5" class="form-control">
                            {{$edit_pro->description}}
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="price">Price:</label>
                        <input type="text" name="price" value="{{$edit_pro->price}}" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="is_top" f>Top:</label>
                        <select name="is_top" id="is_top" class="form-control">
                            <?php
                            $top_1 = '';
                            $top_2 = '';
                            $top_3 = '';
                            if (isset($edit_pro->is_top)) {
                                switch ($edit_pro->is_top) {
                                    case 1:
                                        $top_1 = 'selected';
                                        break;
                                    case 2:
                                        $top_2 = 'selected';
                                        break;
                                    case 3:
                                        $top_3 = 'selected';
                                        break;
                                }
                            }
                            ?>
                            <option value="1" <?php echo $top_1; ?>>1</option>
                            <option value="2" <?php echo $top_2; ?>>2</option>
                            <option value="3" <?php echo $top_3; ?>>3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="on_sale">Sale:</label>
                        <select name="on_sale" id="on_sale" class="form-control">
                            <?php
                            $sale_1 = '';
                            $sale_2 = '';
                            $sale_3 = '';
                            if (isset($edit_pro->on_sale)) {
                                switch ($edit_pro->on_sale) {
                                    case 1:
                                        $sale_1 = 'selected';
                                        break;
                                    case 2:
                                        $sale_2 = 'selected';
                                        break;
                                    case 3:
                                        $sale_3 = 'selected';
                                        break;
                                }
                            }
                            ?>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                    <input type="submit" class="btn btn-primary" name="submit" value="Save"/>
                </form>
            @endforeach
        </div>
    </div>
@endsection
{{--        <div class="o-hidden card shadow">--}}
{{--            <div class="card-body">--}}
{{--                <div class="px-3 py-5 bg-gradient-success text-white">--}}
{{--                    <?php--}}
{{--                    $message = Session::get('message');--}}
{{--                    if($message){--}}
{{--                        echo $message;--}}
{{--                        Session::put('message',null);--}}
{{--                    }--}}
{{--                    ?>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
