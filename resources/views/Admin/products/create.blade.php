@extends('admin_layout')
@section('main-content')

    <div class="container-fluid">
        <div class="add-pro">
                <form method="post" action="{{URL::to('/save-product/')}}" enctype="multipart/form-data" class="add-pro">
                    {{ csrf_field() }}
                    <div class="form-group ">
                        <label for="category_id">Category:</label>
                        <select name="category_id" id="category_id" class="form-control">
                            <?php
                            foreach ($categories as $category):
                            $selected = '';
                            if (isset($edit_pro->category_id) && $category->id == $edit_pro->category_id) {
                                $selected = 'selected';
                            }
                            ?>
                            <option value="<?php echo $category->id ?>" <?php echo $selected; ?>>
                                <?php echo $category->id.'. '.$category->name ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group ">
                        <label for="code">Code:</label>
                        <input type="text" name="code" value="" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" name="name" value="" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="description">Description:</label>
                        <textarea name="description" id="description" cols="" rows="5" class="form-control">

                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="price">Price:</label>
                        <input type="text" name="price" value="" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="is_top">Top:</label>
                        <select name="is_top" id="is_top" class="form-control">
                            <option value="1" >1</option>
                            <option value="2" >2</option>
                            <option value="3" >3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="on_sale">Sale:</label>
                        <select name="on_sale" id="on_sale" class="form-control">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                    <input type="submit" class="btn btn-primary" name="submit" value="Save"/>
                </form>
        </div>
    </div>
@endsection
