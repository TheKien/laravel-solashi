@extends('admin_layout')
@section('main-content')


    <div class="container-fluid">
        <div class="add-cate">
            <h2>Add Category</h2>
            <form method="post" action="{{URL::to('/save-category')}}" enctype="multipart/form-data" class="add-cate">
                {{ csrf_field() }}
                <div class="form-group cate-name">
                    <label>Category name:</label>
                    <input type="text" name="name" value="" class="form-control"/>
                </div>
                <input type="submit" class="btn btn-primary" name="submit" value="Save"/>
            </form>
        </div>
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Created_at</th>
                            <th>updated_at</th>
                            <th>custom</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Created_at</th>
                            <th>updated_at</th>
                        </tr>
                        </tfoot>
                        <tbody>
                            @foreach($all_category as $key => $cate)
                                <tr>
                                    <td>{{$cate->id}}</td>
                                    <td>{{$cate->name}}</td>
                                    <td>{{$cate->created_at}}</td>
                                    <td>{{$cate->updated_at}}</td>
                                    <td>
                                        <a href="{{URL::to('/edit-category/'.$cate->id)}}" title="Sửa">
                                            <i class="fa fa-pencil-alt"></i>
                                        </a>
                                        <a href="{{URL::to('/delete-category/'.$cate->id)}}" title="Xóa"
                                           onclick="return confirm('Are you sure to delete?')">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

{{--    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"--}}
{{--         aria-hidden="true">--}}
{{--        <div class="modal-dialog" role="document">--}}
{{--            <div class="modal-content">--}}
{{--                <div class="modal-header">--}}
{{--                    <h5 class="modal-title" id="exampleModalLabel">edit category</h5>--}}
{{--                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">--}}
{{--                        <span aria-hidden="true">×</span>--}}
{{--                    </button>--}}
{{--                </div>--}}
{{--                <div class="modal-body">--}}
{{--                    <form method="post" action="{{URL::to('/change-category')}}" enctype="multipart/form-data">--}}
{{--                        {{ csrf_field() }}--}}
{{--                        <div class="form-group">--}}
{{--                            <label>Category name:</label>--}}
{{--                            <input type="text" name="name" value="{{$cate->name}}" class="form-control"/>--}}
{{--                        </div>--}}
{{--                        <div class="modal-footer">--}}
{{--                            <input type="submit" class="btn btn-primary" name="submit" value="Save"/>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
@endsection
