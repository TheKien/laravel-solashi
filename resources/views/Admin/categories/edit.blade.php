@extends('admin_layout')
@section('main-content')

    <div class="container-fluid">
        <div class="add-cate">
            <h2>Add Category</h2>
            @foreach($edit_category as $edit_cate)
                <form method="post" action="{{URL::to('/update-category/'.$edit_cate->id)}}" enctype="multipart/form-data" class="add-cate">
                    {{ csrf_field() }}
                    <div class="form-group cate-name">
                        <label>Category name:</label>
                        <input type="text" name="name" value="{{$edit_cate->name}}" class="form-control"/>
                    </div>
                    <input type="submit" class="btn btn-primary" name="submit" value="Save"/>
                </form>
            @endforeach
        </div>
    </div>
@endsection
{{--        <div class="o-hidden card shadow">--}}
{{--            <div class="card-body">--}}
{{--                <div class="px-3 py-5 bg-gradient-success text-white">--}}
{{--                    <?php--}}
{{--                    $message = Session::get('message');--}}
{{--                    if($message){--}}
{{--                        echo $message;--}}
{{--                        Session::put('message',null);--}}
{{--                    }--}}
{{--                    ?>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
