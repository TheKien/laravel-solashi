<!DOCTYPE html>
<html lang="zxx">
   <head>
      <title>RVM SeaMaf | Online &amp; physical bead shop with the best beads and beading supplies in Zimbabwe ✓ Over 9000 beads for jewelry making ✓ Glass beads ✓ Beading supplies and much more!</title>
      <!--meta tags -->
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="" />
      <script>
         addEventListener("load", function () {
         	setTimeout(hideURLbar, 0);
         }, false);
         
         function hideURLbar() {
         	window.scrollTo(0, 1);
         }
      </script>
      <!--//meta tags ends here-->
      <link href="{{ asset('public/frontend/css/all.css') }}" rel="stylesheet" type="text/css" media="all">
      <!--booststrap-->
      <link href="{{ asset('public/frontend/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" media="all">
      <!--//booststrap end-->
      <!-- font-awesome icons -->
      <link href="{{ asset('public/frontend/css/fontawesome-all.min.css') }}" rel="stylesheet" type="text/css" media="all">
      <!-- //font-awesome icons -->

      <!-- owl carousel2 -->
      <link rel="stylesheet" href="{{ asset('public/frontend/css/owl.carousel.min.css') }}">
      <link rel="stylesheet" href="{{ asset('public/frontend/css/owl.theme.default.min.css') }}"> 
      <!-- owl carousel2 -->
      <!-- For Clients slider -->
      <link rel="stylesheet" href="{{ asset('public/frontend/css/flexslider.css') }}" type="text/css" media="all" />
      <!--flexs slider-->
      <link href="{{ asset('public/frontend/css/JiSlider.css') }}" rel="stylesheet">
      <!--Shoping cart-->
      <link rel="stylesheet" href="{{ asset('public/frontend/css/shop.css') }}" type="text/css" />
      <!--//Shoping cart-->
      <!--stylesheets-->
      <link href="{{ asset('public/frontend/css/style.css') }}" rel='stylesheet' type='text/css' media="all">
      <!--//stylesheets-->
      <!-- @livewereStyles -->  
      <!-- <link href="//fonts.googleapis.com/css?family=Sunflower:500,700" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet"> -->
   </head>
<body>
<section>
    <div class="header-outs" id="home">
        <div class="header-bar">
            <div class="container-fluid">
                <div class="hedder-up row">
                    <div class="col-lg-2 col-md-3 logo-head">
                        <a class="navbar-brand" href="index.html"><h4 class="text-logo">RVM SeaMaf</h4></a>
                    </div>
                    <div class="col-lg-5 col-md-6 search-right">
                        <input type="text" name="search" class="header-search" id="header-search"
                                    placeholder="search on PVM Seamaf" >
                    </div>
                    <div class="col-lg-5 col-md-3 right-side-cart">
                        <div class="user-panel">
                            <div class="set-money">
                                <select name="" id="" class="sl-money">
                                    <option value="1">U.S Dollar</option>
                                    <option value="2">RTGS Dollar</option>
                                    <option value="3">SA Rand</option>
                                    <option value="4">British Pound</option>
                                </select>
                            </div>
                            <div class="wishlish">
                                <span class="far fa-heart"></span>
                                <a href="#">Wishlist</a>
                            </div>
                            <div class="cart">
                            <span class="far fa-cart"></span>
                                <a href="#">Shopping Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
            <ul class="navbar-nav ">
                <li class="nav-item active">
                    <a class="nav-link" href="{{URL::to('/trang-chu')}}">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a href="about.html" class="nav-link">Our Shop</a>
                </li>
                <li class="nav-item">
                    <a href="service.html" class="nav-link">On Sale</a>
                </li>
                <li class="nav-item">
                    <a href="shop.html" class="nav-link">Our Services</a>
                </li>
                <li class="nav-item">
                    <a href="Blog.html" class="nav-link">Blog</a>
                </li>
                <li class="nav-item">
                    <a href="contact.html" class="nav-link">Contact</a>
                </li>
                <li class="nav-item">
                    <a href="Signin.html" class="nav-link">Signin</a>
                </li>
                <li class="nav-item">
                    <a href="Signup.html" class="nav-link">Signup</a>
                </li>
            </ul>
        </div>
    </nav>
        </div>
   </div>
   
</section>

   @yield('content')

  <section>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="footer-widget">
                        <h3>about</h3>
                        <p>Online &amp; physical bead shop with the best beads and beading supplies in Zimbabwe
                                 ✓ Over 9000 beads for jewelry making ✓ Glass beads ✓ Beading supplies and much more!</p>
                        <img src="{{'public/frontend/images/cards.png'}}" alt="" class="img-card">
                        <div class="subscribe">
                        <form action="" method="post" class="subscribe-email">
                            <input type="text" name="email" class="form-control" id="email" placeholder="put your e-mail">
                            <button type="submit">Subscribe</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="footer-widget">
                    <h3>Usefuls Links</h3>
                        <div class="user-link">
                            <ul>
                                <li><a href="">About Us</a></li>
                                <li><a href="">Track Orders</a></li>
                                <li><a href="">shipping</a></li>
                                <li><a href="">Contacts</a></li>
                                <li><a href="">My Orders</a></li>
                            </ul>
                            <ul>
                                <li><a href="">Support</a></li>
                                <li><a href="">Terms of Use</a></li>
                                <li><a href="">Privacy Policy</a></li>
                                <li><a href="">Our Services</a></li>
                                <li><a href="">Blogs</a></li>
                            </ul>
                        </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="footer-widget">
                    <h3>Blogs</h3>
                    <div class="post-widget">
                        <div class="post-item">
                            <img src="{{'public/frontend/images/blog-2.jpg'}}" alt="" class="bg-img">
                            <div class="blog-content">
                                <h5 class="blog-text">Write Somethings</h5>
                                <span>3 months ago</span>
                                <a href="" class="readmore">Read more</a>
                            </div>
                        </div>
                        <div class="post-item">
                            <img src="{{'public/frontend/images/blog-1.jpg'}}" alt="" class="bg-img">
                            <div class="blog-content">
                                <h5 class="blog-text">Write Somethings</h5>
                                <span>3 months ago</span>
                                <a href="" class="readmore">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="footer-widget">
                    <h3>Contact</h3>
                        <div class="contact-info">
                            <span>C.</span>
                            <p>RVM SeaMaf</p>
                        </div>
                        <div class="contact-info">
                            <span>B.</span>
                            <p>Ha Dong, Ha Noi</p>
                        </div>
                        <div class="contact-info">
                            <span>T.</span>
                            <p>+123456789</p>
                        </div>
                        <div class="contact-info">
                            <span>E.</span>
                            <p>seamaf@gmail.com</p>
                        </div>
                        <div class="fa-football-ball"></div>
                </div>
            </div>
        </div>
    </div>
     </section>
    <footer class="py-lg-4 py-md-3 py-sm-3 py-3 text-center">
         <div class="social-links-warp">
            <div class="container">
                        <div class="social-links">
                            <div class="social-links">
                                <a href="#" target="_blank" class="instagram"><i class="fab fa-instagram"></i><span>instagram</span></a>
                                <a href="#" target="_blank" class="pinterest"><i class="fab fa-pinterest"></i><span>pinterest</span></a>
                                <a href="#" target="_blank" class="facebook"><i class="fab fa-facebook-f"></i><span>facebook</span></a>
                                <a href="#" target="_blank" class="twitter"><i class="fab fa-twitter"></i><span>twitter</span></a>
                                <a href="#" target="_blank" class="youtube"><i class="fab fa-youtube"></i><span>youtube</span></a>
                                <a href="#" target="_blank" class="linkedin"><i class="fab fa-linkedin"></i><span>linkedin</span></a>
                       </div>
                        <p class="text-white text-center mt-5">Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Developed By <a href="#" target="_blank"></a></p>
      
            </div>
         </div>
    </footer>
      <!-- //footer -->

<!--js working-->
<script src="{{ asset('public/frontend/js/jquery-3.6.0.min.js') }}"></script>
      <script src="{{ asset('public/frontend/js/owl.carousel.min.js') }}"></script>
      <script src="{{ asset('public/frontend/js/script.js') }}"></script>
      <!--//js working-->
      <!-- cart-js -->
      <script src="js/minicart.js"></script>
      <script>
         toys.render();
         
         toys.cart.on('toys_checkout', function (evt) {
         	var items, len, i;
         
         	if (this.subtotal() > 0) {
         		items = this.items();
         
         		for (i = 0, len = items.length; i < len; i++) {}
         	}
         });
      </script>
      <!-- //cart-js -->
      <!--responsiveslides banner-->
      <script src="{{ asset('public/frontend/js/responsiveslides.min.js') }}"></script>
    
      <!--// responsiveslides banner-->	 
      <!-- start-smoth-scrolling -->
      <script src="{{ asset('public/frontend/js/move-top.js') }}"></script>
      <script src="{{ asset('public/frontend/js/easing.js') }}"></script>
      <script>
         jQuery(document).ready(function ($) {
         	$(".scroll").click(function (event) {
         		event.preventDefault();
         		$('html,body').animate({
         			scrollTop: $(this.hash).offset().top
         		}, 900);
         	});
         });
      </script>
      <!-- start-smoth-scrolling -->
      <!-- here stars scrolling icon -->
      <script>
         $(document).ready(function () {
         
         	var defaults = {
         		containerID: 'toTop', // fading element id
         		containerHoverID: 'toTopHover', // fading element hover id
         		scrollSpeed: 1200,
         		easingType: 'linear'
         	};
         	$().UItoTop({
         		easingType: 'easeOutQuart'
         	});
         
         });
      </script>
      <!-- //here ends scrolling icon -->
      <!--bootstrap working-->
      <script src="js/bootstrap.min.js"></script>
      <!-- //bootstrap working-->
      @livewireScripts
   </body>
</html>