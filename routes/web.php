<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// frontend
Route::get('/', 'App\Http\Controllers\HomeController@index');
Route::get('/trang-chu','App\Http\Controllers\HomeController@index');



// backend
Route::get('/admin','App\Http\Controllers\AdminController@index');
Route::get('/dashboard','App\Http\Controllers\AdminController@dashboard');
Route::post('/login','App\Http\Controllers\AdminController@login');
Route::get('/logout','App\Http\Controllers\AdminController@logout');

//category
Route::get('/all-category','App\Http\Controllers\CategoryController@allCategory');
Route::post('/save-category','App\Http\Controllers\CategoryController@saveCategory');
Route::get('/edit-category/{category_id}','App\Http\Controllers\CategoryController@editCategory');
Route::post('/update-category/{category_id}','App\Http\Controllers\CategoryController@updateCategory');
Route::get('/delete-category/{category_id}','App\Http\Controllers\CategoryController@deleteCategory');

//product
Route::get('/all-product','App\Http\Controllers\ProductController@allProduct');
Route::post('/save-product','App\Http\Controllers\ProductController@saveProduct');
Route::get('/create-product','App\Http\Controllers\ProductController@createProduct');
Route::get('/edit-product/{product_id}','App\Http\Controllers\ProductController@editProduct');
Route::post('/update-product/{product_id}','App\Http\Controllers\ProductController@updateProduct');
Route::get('/delete-product/{product_id}','App\Http\Controllers\ProductController@deleteProduct');
