<?php

namespace Database\Factories;

use App\Models\categoris;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategorisFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = categoris::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(2),
            'created_at' => now()
        ];
    }
}
