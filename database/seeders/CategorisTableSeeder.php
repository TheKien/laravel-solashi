<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategorisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\categoris::factory()->count(10)->create();
    }
}
